package com.spleint.farmerline;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,OnMapReadyCallback {

    private double lat = 0.0;
    private double lon = 0.0;
    private SupportMapFragment mapFragment;
    private TextView textView_loc;
    private ImageView imageView_loc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                MainActivity.this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        if (drawer == null){
//            Log.d("Drawer", "onCreate: Drawer is null **-");
//        }
//
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        textView_loc = (TextView) findViewById(R.id.txt_prompt_location);
        imageView_loc = (ImageView)findViewById(R.id.image_loc);

        //Initialize chart
        initGraph();

    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
            super.onBackPressed();
        //}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.select_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this,SelectLocationActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initGraph(){
        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(28000, 0));
        entries.add(new BarEntry(44000, 1));
        entries.add(new BarEntry(13000, 2));

        BarDataSet dataset = new BarDataSet(entries, "# of Calls");

        ArrayList<String> labels = new ArrayList<>();
        labels.add("Instant Sales");
        labels.add("Sales on Credit");
        labels.add("Sales of Saving");

        BarChart barChart = (BarChart) findViewById(R.id.barchart);
        barChart.animateXY(5000,5000);
        barChart.setBackgroundColor(Color.WHITE);
        barChart.setDescription("");
        barChart.setNoDataText("Nothing to show");
        barChart.getAxisRight().setEnabled(false); // no right axis

        Legend legend = barChart.getLegend();
        legend.setEnabled(false);
        // data has AxisDependency.LEFT
        YAxis left = barChart.getAxisLeft();
        left.setDrawLabels(true); // no axis labels
        left.setDrawAxisLine(false); // no axis line
        left.setDrawGridLines(true); // no grid lines
        left.setDrawZeroLine(true); // draw a zero line

        left.setTypeface(Typeface.DEFAULT); // set a different font
        left.setTextSize(12f); // set the text size
        // left.setAxisMinimum(0f); // start at zero
        left.setAxisMinValue(0f);
        left.setTextColor(Color.BLACK);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(12f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(false);
        // set a custom value formatter



        BarData data = new BarData(labels,dataset);
        barChart.setData(data);
    }

    @Override
    protected void onStart() {
        // Get the intent that started this activity
        super.onStart();
        Intent intent = getIntent();
        String action = intent.getAction();
        double lon = intent.getDoubleExtra("coord_result_long",0.0);
        double lat = intent.getDoubleExtra("coord_result_lat",0.0);

        if(intent.getStringExtra("control") != null){
            this.lat = lat;
            this.lon = lon;
            //Register for Map call back
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.display_map);
            mapFragment.getMapAsync(MainActivity.this);
            textView_loc.setText("");
            imageView_loc.setBackground(null);
        }
        else{
            SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
            if (!sharedPreferences.equals(null)){
                double latw = (double)sharedPreferences.getFloat("lat",0.0f);
                double lonw = (double)sharedPreferences.getFloat("lat",0.0f);
                this.lat = latw;
                this.lon = lonw;

                mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.display_map);
                mapFragment.getMapAsync(MainActivity.this);
                textView_loc.setText("");
                imageView_loc.setBackground(null);
            }

            else {
                mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.display_map);
                if(mapFragment != null)
                    getSupportFragmentManager().beginTransaction().remove(mapFragment).commit();
            }
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        LatLng agentLoc = new LatLng(lat, lon);
        float zoomLevel = 16.0f;
        googleMap.addMarker(new MarkerOptions().position(agentLoc)
                .title("Your location"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(agentLoc,zoomLevel));
    }

    public void chooseLocation(View view){
        Intent intent = new Intent(MainActivity.this,SelectLocationActivity.class);
        startActivity(intent);
        finish();
    }
}